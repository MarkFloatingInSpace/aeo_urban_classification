:: opals bat file for preparing the data and exporting it

set AOI="..\Data\input\strip1_AoI.odm"
::set odm_file="%~dp0..\..\data\strip1_AoI.odm"
set odm_file=%AOI%


set grid_file="..\Data\input\DTM_2010.tif"

:: show the content of the data
opalsinfo -inf %odm_file%

:: normalize the heights with the DTM
opalsaddinfo -inf %odm_file% -gridf %grid_file% -attr "normalizedz=z-r[0]"

goto V0
:: V0 .. Normals -searchRadius 3 -neighbours 15 / EchoRatio SR = 3
:: V1 .. Normals -searchRadius 5 -neighbours 15 / EchoRatio SR = 5
:: V2 .. Normals -searchRadius 1 -neighbours 15 / EchoRatio SR = 1
:: V3 .. Normals search radius is defined by mean point distance, EchoRatio searchradius standard: 1


:V0
	:: add linearity, planarity and anisotropy
	opalsNormals -inFile %odm_file% -searchRadius 3 -neighbours 15 -searchMode d3 -storeMetaInfo medium -selmode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 3 -ratioMode slopeAdaptive -maxSigma 0.35

	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "..\Data\pts\AEO_aoi_V0.xyz"
	goto :V1

:V1
	:: add linearity, planarity and anisotropy
	opalsNormals -inFile %odm_file% -searchRadius 5 -neighbours 15 -searchMode d3 -storeMetaInfo medium -selmode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 5 -ratioMode slopeAdaptive -maxSigma 0.35

	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V1.xyz" 
	goto :V2
	

:V2
	:: add linearity, planarity and anisotropy
	opalsNormals -inFile %odm_file% -searchRadius 1 -neighbours 15 -searchMode d3 -storeMetaInfo medium -selmode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 1 -ratioMode slopeAdaptive -maxSigma 0.35

	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V2.xyz" 
	goto :V3

:V3
	:: Default values
	opalsNormals -inFile %odm_file% -neighbours 15 -searchMode d3 -storeMetaInfo medium -selmode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 1 -ratioMode slopeAdaptive -maxSigma 0.35

	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V3.xyz"
