:: opals bat file for preparing the data and exporting it

:batch execution

:set odm file to either strip1 (strip1_AoI) or whole area (aoi)
::set odm_file="%~dp0.\.\data\strip1_AoI.odm"
set odm_file="%~dp0..\..\data\aoi.odm"


:run once if manual execution
::set odm_file=..\..\data\strip1_AoI.odm
::set odm_file=..\..\data\aoi.odm



set grid_file="%~dp0..\..\data\DTM_2010.tif"

:: show the content of the data
opalsinfo -inf %odm_file%

:: normalize the heights with the DTM
opalsaddinfo -inf %odm_file% -gridf %grid_file% -attr "normalizedz=z-r[0]" 
:: -points_in_memory 

goto V0
:: V0 .. Normals -searchRadius 3 -neighbours 15 / EchoRatio SR = 3
:: V1 .. Normals -searchRadius 5 -neighbours 25 / EchoRatio SR = 5
:: V2 .. Normals -searchRadius 1 -neighbours 10 / EchoRatio SR = 1
:: V3 .. Normals search radius is defined by mean point distance, EchoRatio searchradius standard: 1
:: Ground_Classifier try

:Ground_Classifier

	:: ..\..\data\aoi.odm
	opalsAddInfo -inFile %odm_file% -attribute Classification=0
	opalsRobFilter -inFile %odm_file% -filter "echo[last] and generic[echoratio < 4.5]" -sigmaApriori "EchoWidth<3 ? 0.25 : 0.25/(1-(EchoWidth-3)*0.2)"
	
	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_Ground.xyz"
	
	goto :eof


:V0
	:: add linearity, planarity and anisotropy
	opalsNormals -inFile %odm_file% -searchRadius 3 -neighbours 15 -searchMode d3 -storeMetaInfo medium -selMode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 3 -ratioMode slopeAdaptive -maxSigma 0.35
	opalsAddInfo -inFile %odm_file% -attribute Classification=0
	opalsRobFilter -inFile %odm_file% -filter "echo[last] and generic[EchoRatio < 4.5]" -sigmaApriori "EchoWidth<3 ? 0.25 : 0.25/(1-(EchoWidth-3)*0.2)"
	
	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V0.xyz"
	goto :V1

:V1
	:: add linearity, planarity and anisotropy
	opalsNormals -inFile %odm_file% -searchRadius 5 -neighbours 25 -searchMode d3 -storeMetaInfo medium -selMode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 5 -ratioMode slopeAdaptive -maxSigma 0.35
	opalsAddInfo -inFile %odm_file% -attribute Classification=0
	opalsRobFilter -inFile %odm_file% -filter "echo[last] and generic[EchoRatio < 4.5]" -sigmaApriori "EchoWidth<3 ? 0.25 : 0.25/(1-(EchoWidth-3)*0.2)"
	
	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V1.xyz" 
	goto :V2
	

:V2
	:: add linearity, planarity and anisotropy
	opalsNormals -inFile %odm_file% -searchRadius 1 -neighbours 10 -searchMode d3 -storeMetaInfo medium -selMode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 1 -ratioMode slopeAdaptive -maxSigma 0.35
	opalsAddInfo -inFile %odm_file% -attribute Classification=0
	opalsRobFilter -inFile %odm_file% -filter "echo[last] and generic[EchoRatio < 4.5]" -sigmaApriori "EchoWidth<3 ? 0.25 : 0.25/(1-(EchoWidth-3)*0.2)"
	

	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V2.xyz" 
	goto :V3

:V3
	:: Default values
	opalsNormals -inFile %odm_file% -neighbours 15 -searchMode d3 -storeMetaInfo medium  -selMode quadrant
	opalsAddInfo -inFile %odm_file% -attribute "_linearity(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_planarity(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsAddInfo -inFile %odm_file% -attribute "_anisotropy(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
	opalsEchoRatio -inf %odm_file% -searchRadius 1 -ratioMode slopeAdaptive -maxSigma 0.35
	opalsAddInfo -inFile %odm_file% -attribute Classification=0
	opalsRobFilter -inFile %odm_file% -filter "echo[last] and generic[EchoRatio < 4.5]" -sigmaApriori "EchoWidth<3 ? 0.25 : 0.25/(1-(EchoWidth-3)*0.2)"
	

	opalsExport -inFile %odm_file% -oFormat export_format.xml -outFile "out\AEO_aoi_V3.xyz"
	goto :Analysis
	
:Analysis
	opalsHisto -inFile %odm_file% -outParamFile histo.xml -samplerange 200 350 
	opalsHisto -inFile %odm_file% -attribute Amplitude -samplerange 10 200
	opalsHisto -inFile %odm_file% -attribute EchoNumber -samplerange 1 5 -binWidth 1
	opalsHisto -inFile %odm_file% -attribute EchoWidth -samplerange 3 7
	
	opalsHisto -inFile %odm_file% -attribute _linearity
	opalsHisto -inFile %odm_file% -attribute _planarity
	opalsHisto -inFile %odm_file% -attribute _anisotropy -samplerange 0.5 1
	
	opalsHisto -inFile %odm_file% -attribute EchoRatio
	opalsHisto -inFile ..\..\data\aoi.odm -attribute normalizedz -samplerange -5 50
	opalsPointStats -inFile %odm_file% -feature pcount pdens pdist -searchRadius 3
	:: pcount: point count within neighbourhood
	:: pdens: ratio of point count within neighbourhood and the area/volume of search cylinder/sphere
	:: pdist: average linear point distance (2D)
	goto :eof
	
:Pointdensity
	:: Zusammenfassen zu einem File
	:: opalsImport -inf "strip1_AoI.odm" "strip2_AoI.odm" "strip3_AoI.odm" -outf aoi.odm
	:: pdens
	opalsCell -inf %odm_file% -feature pdens -cellsize 1
	opalsZColor -inf aoi_z_pdens.tif -nclasses 10 -zRange 0 30
	
	pointstats -inf %odm_file% -searchRadius 1 -feature pdens 

:not_inplemented, but used for Import/Export
	
	:: export to shp file for inspection in QGIS for inspection
	opalsExport -inFile %odm_file% -outFile "out\strip1.shp" -oFormat fwfShape.xml
	
	:: import points for visual analysis
	opalsimport -inf ..\Data\pts\strip1_classified_DT_v2.xyz -iformat import_format.xml -outf strip1_classified_DT.odm
	opalsimport -inf ..\Data\pts\strip1_classified_NN.xyz -iformat import_format.xml -outf strip1_classified_NN.odm
	
	opalsview -inf strip1_classified_DT.odm
	opalsview -inf strip1_classified_NN.odm
	