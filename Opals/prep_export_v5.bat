:: opals bat file for preparing the data and exporting it

:: Zusammenfassen zu einem File
:: opalsImport -inf "strip1_AoI.odm" "strip2_AoI.odm" "strip3_AoI.odm" -outf aoi.odm

:batch execution
::set AOI="%~dp0..\..\data\aoi.odm"

:set odm file to either strip1 or whole area AOI
set odm_file="..\Data\input\aoi.odm"
::set odm_file=%AOI%


:run once if manual execution (opals version dep)
::set odm_file=..\..\data\strip1_AoI.odm


set grid_file="..\Data\input\DTM_2010.tif"

:: show the content of the data
opalsinfo -inf %odm_file%

:: normalize the heights with the DTM
opalsaddinfo -inf %odm_file% -gridf %grid_file% -attr "normalizedz=z-r[0]"


:: V0 .. Normals -searchRadius 3 -neighbours 15 / EchoRatio SR = 3
:: V1 .. Normals -searchRadius 5 -neighbours 15 / EchoRatio SR = 5
:: V2 .. Normals -searchRadius 1 -neighbours 15 / EchoRatio SR = 1
:: V3 .. Normals search radius is defined by mean point distance, EchoRatio searchradius standard: 1


opalsAddInfo -inFile %odm_file% -attribute Classification=0

:: classify ground vs non ground, adaptive sigma for scab and bushes
opalsRobFilter -inFile %odm_file% -filter "echo[last] and generic[EchoWidth < 4.5]" -sigmaApriori "EchoWidth<3 ? 0.25 : 0.25/(1-(EchoWidth-3)*0.2)"	

::V0
:: add linearity, planarity and anisotropy
opalsNormals -inFile %odm_file% -searchRadius 3 -neighbours 15 -searchMode d3 -storeMetaInfo medium 
opalsAddInfo -inFile %odm_file% -attribute "_linearity_V0(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_planarity_V0(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_anisotropy_V0(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_normalZ_V0 = NormalZ"
opalsEchoRatio -inf %odm_file% -searchRadius 3 -ratioMode slopeAdaptive -maxSigma 0.35
opalsAddInfo -inFile %odm_file% -attribute "_echoratio_V0 = EchoRatio"

::V1
:: add linearity, planarity and anisotropy
opalsNormals -inFile %odm_file% -searchRadius 5 -neighbours 15 -searchMode d3 -storeMetaInfo medium 
opalsAddInfo -inFile %odm_file% -attribute "_linearity_V1(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_planarity_V1(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_anisotropy_V1(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_normalZ_V1 = NormalZ"
opalsEchoRatio -inf %odm_file% -searchRadius 5 -ratioMode slopeAdaptive -maxSigma 0.35
opalsAddInfo -inFile %odm_file% -attribute "_echoratio_V1 = EchoRatio"

::V2
:: add linearity, planarity and anisotropy
opalsNormals -inFile %odm_file% -searchRadius 1 -neighbours 15 -searchMode d3 -storeMetaInfo medium 
opalsAddInfo -inFile %odm_file% -attribute "_linearity_V2(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_planarity_V2(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_anisotropy_V2(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_normalZ_V2 = NormalZ"
opalsEchoRatio -inf %odm_file% -searchRadius 1 -ratioMode slopeAdaptive -maxSigma 0.35
opalsAddInfo -inFile %odm_file% -attribute "_echoratio_V2 = EchoRatio"

::V3
:: Default values
opalsNormals -inFile %odm_file% -neighbours 15 -searchMode d3 -storeMetaInfo medium 
opalsAddInfo -inFile %odm_file% -attribute "_linearity_V3(float)= (NormalEigenvalue1 - NormalEigenvalue2)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_planarity_V3(float)= (NormalEigenvalue2 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_anisotropy_V3(float)= (NormalEigenvalue1 - NormalEigenvalue3)/NormalEigenvalue1"
opalsAddInfo -inFile %odm_file% -attribute "_normalZ_V3 = NormalZ"
opalsEchoRatio -inf %odm_file% -searchRadius 1 -ratioMode slopeAdaptive -maxSigma 0.35
opalsAddInfo -inFile %odm_file% -attribute "_echoratio_V3 = EchoRatio"

opalsExport -inFile %odm_file% -oFormat export_format_v2.xml -outFile "out\AEO_aoi_Vcombined.xyz"

::Analysis
opalsHisto -inFile %odm_file% -outParamFile histo.xml -samplerange 200 350 
opalsHisto -inFile %odm_file% -attribute Amplitude -samplerange 10 200
opalsHisto -inFile %odm_file% -attribute EchoNumber -samplerange 1 5 -binWidth 1
opalsHisto -inFile %odm_file% -attribute EchoWidth -samplerange 3 7

opalsHisto -inFile %odm_file% -attribute _linearity
opalsHisto -inFile %odm_file% -attribute _planarity
opalsHisto -inFile %odm_file% -attribute _anisotropy -samplerange 0.5 1

opalsHisto -inFile %odm_file% -attribute EchoRatio
opalsPointStats -inFile %odm_file% -feature pcount pdens pdist -searchRadius 3
opalshisto -inf %odm_file% -attribute Classification
:: pcount: point count within neighbourhood
:: pdens: ratio of point count within neighbourhood and the area/volume of search cylinder/sphere
:: pdist: average linear point distance (2D)
	
::Pointdensity
opalsCell -inf %aoi% -feature pdens -cellsize 1
opalsAddInfo -inf %aoi% -attribute pdens
opalsZColor -inf aoi_z_pdens.tif -nclasses 10 -zRange 0 30

	