\documentclass[notitlepage]{scrreprt}
\usepackage[utf8]{inputenc}
\usepackage{svg}
\usepackage{amsmath}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{color}
\graphicspath{ {../Opals/}{../Plots/} {../QGIS/}}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage[margin=0.7in]{geometry}
\usepackage{titling}

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{siunitx} % nice formulas
\usepackage{subcaption} % subfigures
\usepackage{booktabs} % nice tables
\usepackage{placeins}
\renewcommand{\arraystretch}{1.2} % more space between table row
\usepackage{lipsum}

\title{Case Study of Urban Full-Waveform \\ 
Airborne Laser Scanning Point Cloud Classification}

\subtitle{Applied Remote Sensing 120.036 TU Wien}

\author{Markus Mikschi 01526165, Andreas Bayr 01526615}
\date{June 2020}

\begin{document}

\begin{titlingpage}
    \maketitle
    \begin{abstract}
        In this report the results of a machine learning based classification of a full-waveform (FWF) airborne laser scanning (ALS) point cloud gathered 2010 of an area near Eisenstadt, Austria are presented. Both a neural network and a decision tree are employed to classify the ALS points into five classes: trees, street, vineyard, flat roof and gable roof. The used features are a combination of inherent FWF ALS values such as echo width as well as values derived from a neighbourhood of points such as echo ratio. A gridsearch with cross-validation is used to obtain well suited hyper-parameters for both algorithms. The training and test datasets are gathered from polygons drawn based on a orthophoto. The results are analysed in terms of fig:confusion between classes, the importance of different features used for the classification, the certainty of the predicted class labels as well as general limitations of the used implementation. 
    \end{abstract}
\end{titlingpage}

\chapter{Introduction}
Monitoring urban regions has enormous economical, sociological and political significance. Cities account for only a small fraction of land coverage but already contain 50\% of today's population with an estimated additional 2.5 billion people by 2050 \citep{UN}. 
Urban remote sensing is a powerful and important tool for gathering the data needed for managing existing resources, planing further urban growth and development. as well as discovering and monitoring existing trends. It also poses unique challenges due to a complex, highly dynamic landscape consisting of a mixture of natural and artificial objects with vastly different material properties. In addition there are further complicating factors such as strong occlusion and cast shadows from tall buildings and trees. 

Airborne laser scanning (ALS) is a remote sensing method that enables to capture the three dimensional nature of cities as point clouds. Full-waveform (FWF) laser scanning provides further information in addition to the point coordinates such as how wide and strong the received echo of a point was. Those attributes allow to infer the reflectivity of objects and if the reflecting object behaved more like a surface reflector (i.e. road) or volume scatter (i.e. tree leaves). With the number of measured points of even relatively small areas easily ranging in the tens of millions, automatic processing of the data is indispensable. One major task of point cloud processing is the classification of points depending on what type of object they represent.

Supervised machine learning offers a wide variety of algorithms well suited for classification tasks, which can also be used for point cloud classification. Decision trees and neural networks, also known as multi layer perceptron classifiers (MLPC), are two examples. In machine learning the performance, measured by a certain metric, for a task (i.e. classification)is improved through experience. In this case the experience is provided in the form of manually labelled points. The algorithm has to find the relationship between the features (i.e. the attributes of the points) and the desired label (i.e. the class index).


{\let\clearpage\relax \chapter{Data and Task Description}}
For this task an FWF ALS point cloud, an orthophoto and a digital terrain model (DTM) were used. Table \ref{tab:data} shows an overview of the used data. The studied area resides in Eisenstadt, Austria and represents sub-urban area with a mixture of houses, roads, trees and grass. A interesting characteristic of the studied area is the presents of vineyards, which are of high economical and cultural significance for the region. Therefore they were captured in a separate class during the classification. The list of the chosen classes can be seen in table \ref{tab:classes}.

The ALS point cloud was recorded in 2010 as three strips using a laser scanner with a rotating mirror, resuling in a quite uniform point density within single strips. Due to the overlap between adjacent strips the point density of the combined point cloud is much larger in those regions. Furthermore areas with vegetation can return multiple echoes and therefore also present with a higher point density. The ALS point cloud has a calculated mean pointdensity of {\SI{23.4}{points/\metre\squared}}, which was computed with \emph{opals pointstats} with a search radius of 0.56 meters which resulted in a search area of approximatly {\SI{1}{\metre\squared}}.

The DTM has a grid size of one meter and was used to obtain the normalized point height above ground. The ortophoto was used for a first assessment of the region in which a determination of potential features along with relevant classes for this region was conducted. In a second step it was used for manually creating the training and test datasets for the classification by drawing polygons. This process is described in section \ref{sec:training_data}. Figure \ref{tab:data} shows studied area as the ortophoto overlayed with the point density of the point cloud. 

\begin{table}[ht!]
	\centering
	\caption{Used Data from Eisenstadt, Burgenland, ETRS89/UTM zone 33N}
	\begin{tabular}{|l | l | l |}
    	\hline
		Data & Format & Date of recording\\
		\hline
        ALS point clouds, 3 strips, ca. 40Mio Points & ODM & 04-2010 to 07-2010\\
		RGB Orthophoto (visual analysis) & GEOTIFF & - \\
		Digital Terrain Model (DTM) & GEOTIFF, 1m gridsize & - \\
		\hline
	\end{tabular}
	\label{tab:data}
\end{table}


\begin{figure}[ht!]
\centering
\includegraphics[width=0.8\textwidth]{AEO_orthophoto_pdens}
\caption{Orthophoto with Point Density}
\label{fig:pdens}
\end{figure}

\begin{table}[ht!]
	\centering
	\caption{Classes}
	\begin{tabular}{|l | l |}
    	\hline
		Number & Class\\
		\hline
		1 & Forrest\\
		2 & Meadow \\
        3 & Street\\
        4 & Wineyard\\
		5 & Roof flat\\
		6 & Roof normal\\
		\hline
	\end{tabular}
	\label{tab:classes}
\end{table}


{\let\clearpage\relax \chapter{Methods}}

\section{Training Data}
\label{sec:training_data}

Both the Decicion Tree (DT) and the Neuronal Network (NN) are supervised learning algorithms and therefore need correctly labelled data to be trained on. This data is also called ground truth. In the case of this study is was manually defined by drawing polygons in QGIS \citep{QGIS} based on the orthophoto. Figure \ref{fig:class_polygons} shows the manually classified regions. Those polygons were subsequently used to assign a class label to all points within them. However, due to inconsistencies between the time of recording the orthophoto and the ALS data some points are labelled incorrectly. Furthermore ALS captures some ground points beneath vegetation. In order to remove such points from the \textit{tree} and \textit{wineyard} classes and to reduce the number of wrongly labelled points in general an additional step was used. A Robfilter in Opals \citep{OPALS} was employed to classify the point cloud in terrain and off-terrain points. Points whose classification was inconsistent with their class, such as ground points in the tree class, were removed from the labelled dataset.

The abundance of trees in the studied area and the higher point density of those areas caused by multiple echoes led to a strong imbalance of the class distribution. It was observed that this causes a bias in the classifier as it makes it generally safer to predict that a point belongs to the dominant class since that class represents a much larger part of the training and test point population. Therefore the number of tree points was artificially reduced by randomly sampling so that the tree class contained a comparable number of points to the other classes. The class distribution of the used points can be seen in Figure \ref{fig:class_distribution}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.8\textwidth]{ortho_polys}
	\caption{Orthophoto with Classification Training Polygons}
	\label{fig:class_polygons}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.7\textwidth]{NN_v2/number_obs_per_class}
	\caption{Distribution of reduced ground truth data}
	\label{fig:class_distribution}
\end{figure}


\section{Pointcloud Attributes}
The most important information of point clouds is the relative position of points to one another. Simple machine learning algorithms cannot take advantage of this information directly. In the two dimensional case of images convolutional neural networks (CNN) are used to extract information from the relationship of neighbouring pixels. Extending this technique to the three dimensional, non uniformly sampled case of point clouds is a topic of ongoing research and is not further examined in this work. In order to still obtain information from the relative point positions, point features based on a neighbourhood of points are calculated using OPALS.

The Level of Aquisition (LoA) describes the level of information of the data in a point cloud. L0 data refers to the measuring process directly (e.g. point coordinates, amplitude), while L1 data is calculated using data of individual points (e.g. reflectance based on normalizing the amplitude). L2 Data is calculated from the pointwise neighbourhood (e.g. echo ratio, normal vector) and L3 data uses other datasets such as the DTM for deriving the normalized height above ground. Table  \ref{tab:attributes} shows all used features for the classification along with their LoA.

\begin{table}[ht!]
	\centering
	\caption{Attributes and Features from the Pointcloud}
	\begin{tabular}{|c |l | l |}
    	\hline
		LoA & Attribute & Origin / Function \\
		\hline
		L0 & Point coordinates & FWF data\\
		L0 & Amplitude & FWF Data \\
		L0 & Echo Width & FWF Data\\
		L0 & Echo Number & FWF Data\\
		L0 & Number of Echoes & FWF Data\\
		L1 & Reflectance & FWF Data\\
        L2 & Echo Ratio & \emph{opalsEchoRatio}\\
		L2 & GroundClassification & \emph{opalsRobFilter}\\
		L2 & NormalZ & \emph{opalsNormals}\\
		L2 & Linarity & \emph{opalsNormals} + \emph{opalsAddInfo}\\
		L2 & Planarity & \emph{opalsNormals} + \emph{opalsAddInfo}\\
		L2 & Anisotropy & \emph{opalsNormals} + \emph{opalsAddInfo}\\
		L3 & Normalized Z & Difference to DTM File (OPALS)\\
		\hline
	\end{tabular}
	\label{tab:attributes}
\end{table}

\subsection{Calculated L2 features}
The following features were calculated in OPALS from a neighbourhood of each individual point in order to capture some of the information contained in the relative point positions.

\begin{itemize}
	\item \textbf{Echo ratio}\\
	The echo ratio is computed for every point individually by dividing the number of points contained in a spherical neighbourhood by the number of points contained in a vertical cylinder with the same radius. This value is sensitive to big differences in the vertical position of points which are close to each other in the horizontal directions. For flat surfaces the ratio is close to one, while for trees and the edges of a roof the number is closer to zero. Multiple settings for the search radius were tested and two different versions with 1 m and 5 m were selected as features in order to capture different aspects of the point distribution. Those two versions are denoted as V1 and V2 respectively. The values were calculated in OPALS using the \emph{opalsEchoRatio} function while using the slopeAdaptive option. This option fits a plane through the points in the spherical neighbourhood and under the condition the quality sigma of the fit is under 0.35 the search radius of the spherical neighbourhood is expanded so that the sphere goes through the intersection of the fitted plane and the cylinder.
	
\item \textbf{Normal vector}\\
The normal vector was calculated for each individual point from its neighbourhood as defined by a fixed number of neighbours and maximum search radius. A neighbourhood of 15 points in a sphere with 1 and 5 meter respectively was used to fit a plane and calculate its normal vector using the \emph{OpalsNormals} command. The versions are refered to as V1 and V2. If not all 15 neighbours are found in within the maximum search radius, the calculations are executed with the found points. Only the z-component of the normal vector was used as a feature during the classification. It is related to the inclination of the fitted plane but independent from its azimuthal orientation.

\item \textbf{Lineartity, planarity and anisotropy} \\
Based on the three eigenvalues from the principal component analysis (PCA) used during the estimation of the normal vector three more features, linearity, planarity and anisotropy, were calculated. These are normalized values ranging from 0 to 1 which describe the point distribution of the used neighbourhood. They were calculated using the eigenvalues calculated during the call of the \emph{opalsNormals} functions with the \emph{opalsAddInfo} function. Since two different versions of the normal vector calculation were used, two different sets of eigenvalues were available. Therefore the linearity, planarity and anisotropy were also calculated in two versions, denoted V1 and V2. In the following equations $\lambda_1$, $\lambda_2$ and $\lambda_3$ denote the eigenvalues sorted in descending order.

\begin{equation}
linearity = \frac{\lambda_1 - \lambda_2}{\lambda_1}
\end{equation}
\begin{equation}
planarity = \frac{\lambda_2 - \lambda_3}{\lambda_1}
\end{equation}
\begin{equation}
anisotropy = \frac{\lambda_1 - \lambda_3}{\lambda_1}
\end{equation}
	
\end{itemize}



\section{Model tuning}
Both the decision tree and the neural network have so called hyper-parameters which have to be set before training the model. Those parameters refer to how the model is shaped, built or trained and therefore are different from the learned parameters themselves. A common method of determining good values for those parameters is to try multiple and choose the best performing ones as measured by a certain performance metric. This method is referred to as grid search as it tests all combination of values for the different hyper-parameters, resulting in a grid in the parameter-space.

Performance measures should always be calculated as out-of-sample, meaning on data that was not used to train the model. This is to avoid overfitting and get a more realistic estimate of the model performance. In order to accomplish this the data is first split into a training- and a test set, with the test set only being used once after the model is completely trained. In this study a 60\%/40\% split was used. For calculating the performance metric during the grid search a part of the training set is set aside as a so-called validation set. The model is trained on the remaining test set and then evaluated on the validation set. In order to get multiple out-of-sample performance estimates cross-validation is often used in conjunction with grid search. In cross-validation the training set is split into k folds and each fold is used as the validation set, while the remaining folds are used to train the model. This yields k estimates of the model performance which can be averaged to obtain a more reliable estimate. In this study a cross-validation with five folds was used during the grid search.

The choice of the performance metric is largely dependent on the task and in particular the cost and risk associated with false positives and false negatives. In this case there is no higher cost of false positives or false negatives, and the model should just work well over all. Therefore the weighted F1-score is used. The F1-score is for binary classification and is computed as the harmonic mean between the precision and recall of the model. Precision is the fraction of the true positives of true and false positives (see Eq. \ref{precision}). Recall on the other hand is the fraction of true positives of true positives and false negative (see Eq. \ref{recall}).

\begin{equation} \label{precision}
precision = \frac{TP}{TP + FP}
\end{equation}
\begin{equation} \label{recall}
recall = \frac{TP}{TP + FN}
\end{equation}
\begin{equation} \label{f1}
F1 = \frac{precision + recall}{2 \sqrt{precission \cdot recall}}
\end{equation}

\begin{equation} \label{f1_avg}
F1_{weighted} = \frac{\sum F1_i \cdot support_i}{\sum support_i}
\end{equation}

The harmonic mean returns values similar to the arithmetic mean if the inputs are similar, but returns a value closer to the lower value if the values are dissimilar. This creates a performance metric that emphasises the importance of both good recall and precision in a model. In order to expand the F1-score to a classification with more than two classes, the score is calculated individually for each label and subsequently combined in a weighted average. The support of the different classes, which is the number of true data points of this class, is used as weights, as can be seen in Eq. \ref{f1_avg}.
 
\subsection{Neural Network}
Neural networks can learn complex relations between input and output variables by tuning the weights of associated connections between so called neurons, mimicking natural brains. The network is organised in layers with one or more neurons. The first layer is the input layer and the neurons within it adopt the values of the features of the current sample. The neurons of one layer are connected with the neurons in the next layer by weighted connections. The neuron values of the previous layer are multiplied by the respective weights and summed up at the receiving neurons in the next layer. The neurons apply an activation function to the sum who's result becomes the output of the neuron. This process is repeated until the last layer is reached. That last layer is the output layer with one neuron per class or value to be predicted. In supervised machine learning the expected output of the output layer is known and the weights are iteratively changed in order to minimize the discrepancies between the expected and the predicted results as measured by some error measure.

There are multiple possible activation functions with the most common being sigmoid, tanh and ReLU. Those three options were tested using a grid search with cross-validation but were found to have very little impact on this specific classification task. The tanh was ultimately chosen as it resulted in a slightly better weighted F1-score.

While the number of input and output neurons is determined by the number of features and classes respectively, the number of hidden layers and the number of neurons with them can be chosen freely. More complex neural networks with more hidden layers and more neurons are able to learn more complex relations between the inputs and outputs. This specific field is called deep learning. However, a too complex model for any given task is prone to over-fitting. Therefore an ideal architecture has to be found for every task. Different network architectures were tested together with the regularization parameter alpha in a grid search.

Regularization is used to avoid large weights, placing too much emphasise on single features and to avoid over-fitting. A term is introduced into the cost functions which is minimized during training. Here the squared length of the weight vector is used. The regularization factor alpha determines the penalty associated with this value. Large values of alpha constrain the network too much and the underlying relation between the input and the output cannot be captured with sufficient accuracy. Too small values might lead to over-fitted models. Different values for alpha were tested along with different network architectures as described in the previous paragraph.

Figure \ref{fig:NN_tuninggrid} shows the mean weighted F1-scores obtained from a five fold cross-validation in a grid search, testing the different network architectures and different regularization penalties. The best performing option is a neural network with two hidden layers with 15 and 13 neurons respectively and a regularization penalty of 0.001. 



\begin{figure}[ht!]

\centering
\includegraphics[width=0.6\textwidth]{NN_v2/grid_search_F1_matrix}
\caption{Weighted F1-scores for the different hyper-parameters for the NN on a custom tuning grid}
\label{fig:NN_tuninggrid}
\end{figure}

\subsection{Decision tree}
Decision Trees (DT) are a non-parametric supervised learning method that can be used for classification and regression. The prediction of the value of a target variable is achieved by learning simple decision rules inferred from the data features. One major benefit of decision trees over neural networks is that they are easy to understand and to interpret, in contrast to black box models such as NN. The resulting decision tree can even be visualised. 


DT tend to overfit and therefore methods such as pruning, setting the minimum number of samples required at a leaf node or setting the maximum depth of the tree are necessary to avoid over fitting and obtain a classifier that generalizes well to unseen data. In this work the maximum depth is limited during the training, with threshold being varied in a grid search in order to strike a balance between capturing the true structure of the data and avoid over fitting.

that entropy could be used to describe the amount of unpredictability in a random variable.

Entropy is used when determining how much information is encoded in a particular decision. This information gain is useful when, upon being presented with a set of attributes about your random variable, you need to decide on which attribute tells you the most info about the variable.

placing attributes with the highest information gain at the top of the tree will lead to the highest quality decisions being made first. This will result in more succinct and compact decision trees.


Similar to entropy, which had the concept of information gain, gini gain is calculated when building a decision tree to help determine which attribute gives the most information about which class a new data point belongs to.

This is done in a similar way to how information gain was calculated for entropy, except instead of taking a weighted sum of the entropies of each branch of a decision, we take a weighted sum of the gini impurity.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.6\textwidth]{DT_v2/grid_search_F1_matrix}
\caption{Weighted F1-scores for the different hyper-parameters for the DT on the custom tuning grid}
\end{figure}

{\let\clearpage\relax \chapter{Results}}

After the hyper-parameters of both the decision tree and the neural network were tuned, the algorithms were trained on the entire training set and subsequently tested on the test set.

It is important to note that the training and test sets are no perfect representation of the studied area, as some challenging situation could not be mapped with polygons based on an orthophoto. Such situation include the precise edges and transitions between two adjacent classes such as roof edges, ground points beneath trees, points on house walls and more. Therefore the obtained scores most likely represent a too optimistic view on the performance.

Both the decision tree and the neural network perform reasonably well with the decision tree slightly outperforming the neural network for every class. Figure \ref{fig:confusion} shows the confusion matrices of both models. There are two notable classification problems for both algorithms. One the one hand the distinction between grass and street seems to be difficult. This is evident as both classes are mostly horizontal planes at ground level, resulting in similar values for a lot of features such as normalized height, planarity and echo ratio. A key differentiating feature is the reflectivity which is a unique characteristic of FWF laser scanning compared to regular laser scanning. However, the reflectivity values of the two classes also have a large overlap as can be seen in Figure \ref{fig:grass_vs_street}. Here the possibilities of a classification solely based on point cloud data are very limited. An augmentation with an additional dataset such as NDVI (normalized difference vegetation index) could improve these results.

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{DT_v2/confusion_matrix_DT}
	\end{subfigure}
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{NN_v2/confusion_matrix_NN}
	\end{subfigure}
	\caption{Confusion Matrizes from a) DT and b) NN}
	\label{fig:confusion}
\end{figure}

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{Comparing_features/Planarity_V0_grass_street}
		\end{subfigure}
	\begin{subfigure}[b]{0.48\textwidth}
\includegraphics[width=\textwidth]{Comparing_features/reflectivity_grass_street}
\end{subfigure}
	\caption{Comparison of the planarity and reflectivity features for the grass and street classes}
	\label{fig:grass_vs_street}
\end{figure}
	

The second issue is the distinction between flat and gable roofs. Those two classes present with very similar values for most features just like the grass and tree classes. Normalized height, planarity, echo ratio and reflectivity are all similar for both roof types. The Z-component of the normal vector, indicating the slope of the surface, is the best suited feature for differentiating the two classes. However, as can be seen in Fig \ref{fig:roofs_normalz} the distributions of this features for both classes have a significant amount of overlap. The spike of values close to 1.0 for the gable roof class is at least partially because of points close to the crown of the roofs. Due to the symmetrical point distribution around the roof crown, the fitted plane is close to horizontal, resulting in the z-component of the normal vector being close to 1.0. This causes some points close to the crown of the roof to be labeled as flat roof. This can be seen in Fig \ref{fig:NN_example_w_proba}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\textwidth]{Comparing_features/NormalZ_V0_flat_gable}
	\caption{Comparison of the distribution of z-component values of the normal vector between flat and gable roofs}
	\label{fig:roofs_normalz}
\end{figure}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.8\textwidth]{NN/NN_classification_proba}
	\caption{Comparison of the distribution of z-component values of the normal vector between flat and gable roofs}
	\label{fig:roofs_normalz}
\end{figure}




\subsection{Feature Importance}
It is possible to assess the importance of the different features used for the classification task by calculating the permutation feature importance. In this procedure the values of one feature at a time are shuffled randomly and the difference in performance as measured by a specific performance measure is computed by classifying the altered dataset. As the shuffling is performed randomly the procedure is repeated multiple times and the resulting loss in performance is averaged to obtain a more reliable estimation of the feature importance. In this study 20 iterations were used along with the weighted F1-score as the performance measure. The results for both the decision tree and the neural network are shown in Fig \ref{fig:feature_importance}. The scattering of the individual iterations was quite low so that only the resulting averages are plotted.

As can be seen, the top three most important features are identical for both algorithms. However, the decision three is more reliant on those features because they are most likely used in early decisions in the decision tree. The normalized height above ground (NormalizedZ) is evidently the most important feature as it enables to distinguish between classes that present otherwise with very similar features such as streets and flat roofs or vineyards and trees. The feature importance of the echo width and to a lesser degree that of reflectance show the benefit of using FWF laser scanning data over regular laser scannig data. Furthermore the neural network shows a more steady decline in feature importance over the different features, while there is a more drastic difference in the importance in the third and fourth most important feature for the DT. The higher importance of the z-component of the normal vector (NormalZ) for the DT is consistent its better performance in differentiating flat and gable roofs. The NN might not have identified this features as key factor during training. The low importance of 

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{DT_v2/feature_importance_DT}
	\end{subfigure}
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{NN_v2/feature_importance_NN}
	\end{subfigure}
	\caption{Mean permutation feature importance with 20 iterations for the DT (left) and NN (right)}
	\label{fig:feature_importance}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=1\textwidth]{Analysis/certanty}
\caption{}
\end{figure}


\begin{figure}[htbp]
	\centering
		\includegraphics[width=\textwidth]{Analysis/Example2/NN}

\includegraphics[width=\textwidth]{Analysis/Example2/NN_proba}

	\caption{Example of NN labeled point cloud along with the certainty of the NN for each point.}
	\label{fig:NN_example_w_proba}
\end{figure}


\FloatBarrier
\section{Conclusion}


The discussed errors and general issues of the classification give insight in key aspects of point cloud classification. On the one hand they highlight the limitation of simple approaches such as the used neural network and decision tree and therefore demonstrate the need of development of more sophisticated methods that take better advantage of information encoded in relative point position. On the other hand the importance of correct and complete ground truth datasets becomes clear. Manual labeling based on orthophotos has strong limitations due to occlusion of ground points or not visible points such as on walls. Furthermore it is difficult to capture the borders between two adjacent classes and a trade-off between a mostly correct ground truth, avoiding labeling borders, and a mostly complete ground truth, trying to lable classes close to their borders, has to be made. A potential solution to the latter problem is the use of synthetic ground truth. Advances in computer hardware and software have made it possible to efficiently simulate large measurement datasets based on a modeled environment. Those datasets can be used to train and test a model and since the measurements are based on a simulation the corresponding labels are known with perfect accuracy. This approach is already used in for example computer vision in the context of self-driving cars.


\bibliographystyle{plain}
\begin{thebibliography}{10}

\bibitem{UN}  World Urbanization Prospects: The 2018 Revision: key facts, population.un.org/wup/Publications/Files/WUP2018-KeyFacts.pdf, 2018, last visited 15.05.2020


\bibitem{QGIS} QGIS.org (YEAR). QGIS Geographic Information System. Open Source Geospatial Foundation Project. http://qgis.org

\bibitem{OPALS} N. Pfeifer, G. Mandlburger, J. Otepka, W. Karel: OPALS - A framework for Airborne Laser Scanning data analysis. Computers, Environment and Urban Systems, 45 (2014), 125 - 136

\end{thebibliography}



%
% \section*{A - Opals Batch File}
% \lstset{
%     language=command.com,
%     tabsize=3,
%     frame=lines,
%     caption=Opals Batch File,
%     label=code:opals,
%     %frame=shadowbox,
%     rulesepcolor=\color{gray},
%     xleftmargin=20pt,
%     framexleftmargin=15pt,
%     keywordstyle=\color{blue}\bf,
%     commentstyle=\color{OliveGreen},
%     stringstyle=\color{red},
%     numbers=left,
%     numberstyle=\tiny,
%     numbersep=5pt,
%     breaklines=true,
%     showstringspaces=false,
%     basicstyle=\footnotesize,
%     emph={food,name,price},emphstyle={\color{magenta}}}
% \lstinputlisting{../Opals/prep_export_v5.bat}
%
%
% \section*{B - Export Format}
% \lstset{
%     language=xml,
%     tabsize=3,
%     frame=lines,
%     caption=Export\_Format.xml,
%     label=code:sample,
%     %frame=shadowbox,
%     rulesepcolor=\color{gray},
%     xleftmargin=20pt,
%     framexleftmargin=15pt,
%     keywordstyle=\color{blue}\bf,
%     commentstyle=\color{OliveGreen},
%     stringstyle=\color{red},
%     numbers=left,
%     numberstyle=\tiny,
%     numbersep=5pt,
%     breaklines=true,
%     showstringspaces=false,
%     basicstyle=\footnotesize,
%     emph={food,name,price},emphstyle={\color{magenta}}}
% \lstinputlisting{../Opals/export_format.xml}
%
%
%
% \section*{C - Distribution of Pointcloud Attributes}
%
% \begin{figure}[htbp]
% 	\centering
% 	\begin{subfigure}[b]{0.48\textwidth}
% 		\includegraphics[width=\textwidth]{aoi_z_histo}
% 		\caption{Height}
% 	\end{subfigure}
% 	\begin{subfigure}[b]{0.48\textwidth}
% 		\includegraphics[width=\textwidth]{aoi_normalizedz_histo}
% 		\caption{Normaized Height}
% 	\end{subfigure}
% 	\begin{subfigure}[b]{0.48\textwidth}
% 		\includegraphics[width=\textwidth]{aoi_Amplitude_histo}
% 		\caption{Amplitude}
% 	\end{subfigure}
% 	\begin{subfigure}[b]{0.48\textwidth}
% 		\includegraphics[width=\textwidth]{aoi_EchoNumber_histo}
% 		\caption{Echo Number}
% 	\end{subfigure}
% 	\begin{subfigure}[b]{0.48\textwidth}
% 		\includegraphics[width=\textwidth]{aoi_EchoRatio_histo}
% 		\caption{Echo Ratio}
% 	\end{subfigure}
% 	\begin{subfigure}[b]{0.48\textwidth}
% 		\includegraphics[width=\textwidth]{aoi_EchoWidth_histo}
% 		\caption{Echo Width}
% 	\end{subfigure}
%
% 	\caption{\textbf{Opals Histograms}}
% 	\label{fig:opals_histo}
% \end{figure}

\end{document}
