import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np

pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_combined_versions.xyz"

pc_df = pd.read_csv(pc_path, delimiter="\t", skiprows=1,
                    names=["x", "y", "z", "EchoWidth",
                           "EchoNumber", "NrOfEchos", "Reflectance",
                           "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                           "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                           "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                           "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                           "NormalizedZ", "Ground_classification"])

multi_features = ["Linearity", 'Planarity', 'Anisotropy', 'NormalZ', 'EchoRatio']

fig, axes = plt.subplots(2,3)

for idx, curr_multi_feature in enumerate(multi_features):
    curr_feature_names = [curr_multi_feature + "_V" + str(i) for i in range(0, 4)]

    corr = pc_df[curr_feature_names].corr()

    # Using matshow here just because it sets the ticks up nicely. imshow is faster.
    axes[np.unravel_index(idx, (2,3))].matshow(corr, cmap='winter_r', vmin=0.6, vmax=1)

    for (i, j), z in np.ndenumerate(corr):
        axes[np.unravel_index(idx, (2,3))].text(j, i, '{:0.2f}'.format(z), ha='center', va='center')

    axes[np.unravel_index(idx, (2, 3))].set_title(curr_multi_feature)