import pandas as pd
import os
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import scale
import time

import concurrent.futures

# pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\AEO_aoi_Vcombined.xyz"
pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\all_strips_combined_versions.xyz"
shp_path = r"Y:\AEO_urban_classification\aeo_urban_classification\QGIS\combined2.shp"
out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts"

# ----------------------

# read point cloud
pc_df = pd.read_csv(pc_path, delimiter="\t", skiprows=1, usecols=[0, 1, -2], names=["x", "y", "Ground_classification"])

# thin out point cloud
point_batch_indices = range(0, pc_df.shape[0], 1)
pc_df = pc_df.iloc[point_batch_indices]
# pc_df['index1'] = pc_df.index
pc_df.reset_index(inplace=True)

# read the polygons form shapefile
polys = gpd.read_file(shp_path)
classes = polys.id.unique()
combined_polys = {}

# combine polygons
for current_class in classes:
    # combine all polygons of a certain class (id)
    combined_polys[current_class] = polys.loc[polys['id'] == current_class].geometry.unary_union

number_batches = 500
n_points = pc_df.shape[0]
batch_boundary_indices = np.linspace(0, n_points, number_batches + 1, dtype=int, endpoint=True)
print(batch_boundary_indices)
# ----------------------


# define the point labeling procedure to be run in parallel
def procedure(batch_idx):
    print("Batch ", batch_idx + 1, "/", number_batches)

    current_point_indices = range(batch_boundary_indices[batch_idx], batch_boundary_indices[batch_idx + 1])

    # create a GeoDataFrame from the x and y coordinates of the PC
    pc_gdf = gpd.GeoDataFrame(geometry=gpd.points_from_xy(pc_df.loc[batch_boundary_indices[batch_idx]:batch_boundary_indices[batch_idx + 1]-1, 'x'],
                                                          pc_df.loc[batch_boundary_indices[batch_idx]:batch_boundary_indices[batch_idx + 1]-1, 'y']))

    curr_point_classification = np.zeros([pc_gdf.shape[0], 1], int)

    # iterate over all classes in the shapefile
    for current_class in classes:
        # get the indices of the points within said combined polygon
        idx = pc_gdf.geometry.within(combined_polys[current_class])
        if sum(idx) > 0:
            curr_point_classification[idx, 0] = np.ones(sum(idx)) * current_class

    print(len(current_point_indices), ' ', curr_point_classification.shape)
    return current_point_indices, curr_point_classification


# execute the labeling in parallel
def run_pool(max_workers=4):
    # create a numpy vector to store the labels, zero means no label
    pt_classification = np.zeros([pc_df.shape[0], 1], int)

    print('Starting parallel processing')
    start = time.time()

    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        for point_batch_indices, point_classification in executor.map(procedure, range(0, number_batches)):
            pt_classification[point_batch_indices] = point_classification

    end = time.time()
    return pt_classification


pt_classification = run_pool()
pc_df["label"] = pt_classification
pc_df = pc_df.loc[pc_df["label"] != 0]

pc_df.to_csv(os.path.join(out_path, "df_all_strips_labels.csv"))
