import pandas as pd
import os
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import scale

import concurrent.futures

pc_path = r"Y:\2_2_Urban_Full_Waveform_Classification\AEO_data.xyz"
shp_path = r"Y:\AEO_urban_classification\aeo_urban_classification\QGIS\combined2.shp"
out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data"

# read point cloud
pc_df = pd.read_csv(pc_path, delimiter="\t", skiprows=1, usecols=[0,1], names=["x", "y"])

pc_df['class'] = 0

# ---------------------------------------
# try to process the data in batches
batch_number = 100

for batch_idx in range(batch_number):
    print("Batch ", batch_idx+1, "/", batch_number)

point_batch_indices = range(0, pc_df.shape[0], 10)
pc_df = pc_df.iloc[point_batch_indices]

# create a GeoDataFrame from the x and y coordinates of the PC
pc_gdf = gpd.GeoDataFrame(geometry=gpd.points_from_xy(pc_df.x, pc_df.y))

# read the polygons form shapefile
polys = gpd.read_file(shp_path)
classes = polys.id.unique()
point_classification = np.zeros([pc_gdf.shape[0], 1], int)

# iterate over all classes in the shapefile
for current_class in classes:
    # combine all polygons of a certain class (id)
    current_combined_poly = polys.loc[polys['id'] == current_class].geometry.unary_union
    # get the indices of the points within said combined polygon
    idx = pc_gdf.geometry.within(current_combined_poly)
    if sum(idx) > 0:
        point_classification[idx, 0] = np.ones(sum(idx)) * current_class

# add class label to df
pc_df.loc[point_batch_indices, ['class']] = np.squeeze(point_classification)

# drop all none classified points
pc_df = pc_df.loc[pc_df['class'] != 0]

# add relative echo number
pc_df['RelEchoNumber'] = pc_df['EchoNumber'] / pc_df['NrOfEchos']

# save pandas df as csv
pc_df.to_csv(os.path.join(out_path, "labeled_points_raw.csv"))

# show the number of points per class
class_names = ['Wald', 'Wiese', 'Straße', 'Weingärten', 'Häuser flach', 'Häuser Giebel']
class_pts_count = pc_df['class'].value_counts()
false_trees = sum((pc_df['class'] == 1) & (pc_df['NormalizedZ'] < 1))

plt.figure()
plt.bar(class_pts_count.index, class_pts_count)
plt.xticks(range(1, len(class_names)+1), class_names)
plt.bar(1, false_trees)
plt.show()