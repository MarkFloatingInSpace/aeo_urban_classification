import pandas as pd
import os
import matplotlib.pyplot as plt
import geopandas as gpd
from geopandas.tools import sjoin
from shapely.geometry import Point, Polygon
import descartes
import numpy as np
from sklearn.neighbors import KernelDensity
from mpl_toolkits.mplot3d import Axes3D
from os.path import join
import itertools

labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\all_strips_combined_clean.csv"
plot_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Plots\Comparing_features"

type_dict = {'x': 'float32', 'y': 'float32', 'z': 'float32', "EchoWidth": 'float32', "EchoNumber": 'int8',
             "NrOfEchos": 'int8', "Reflectance": 'float32',
             "Linearity_V0": 'float32', "Planarity_V0": 'float32', "Anisotropy_V0": 'float32', "NormalZ_V0": 'float32',
             "EchoRatio_V0": 'float32',
             "Linearity_V1": 'float32', "Planarity_V1": 'float32', "Anisotropy_V1": 'float32', "NormalZ_V1": 'float32',
             "EchoRatio_V1": 'float32',
             "Linearity_V2": 'float32', "Planarity_V2": 'float32', "Anisotropy_V2": 'float32', "NormalZ_V2": 'float32',
             "EchoRatio_V2": 'float32',
             "Linearity_V3": 'float32', "Planarity_V3": 'float32', "Anisotropy_V3": 'float32', "NormalZ_V3": 'float32',
             "EchoRatio_V3": 'float32',
             "NormalizedZ": 'float32', "Ground_classification": 'int8', 'label': 'int8'}

labeled_pc_df = pd.read_csv(labeled_pc_path, delimiter=",",
                            usecols=["x", "y", "z", "EchoWidth",
                                     "EchoNumber", "NrOfEchos", "Reflectance",
                                     "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                                     "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                                     "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                                     "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                                     "NormalizedZ", "Ground_classification", "label"], dtype=type_dict)

print(labeled_pc_df.shape)

# thinning of pc
labeled_pc_df = labeled_pc_df.iloc[range(0, labeled_pc_df.shape[0], 100)]

number_subplot_columns = 3
# ------------------------------ Plot certain features as kernel density estimation ------------------------------------
features = [["Linearity_V1", "Linearity_V2", "Linearity_V3"],
            ["Planarity_V1", "Planarity_V2", "Planarity_V3"],
            ["Anisotropy_V1", "Anisotropy_V2", "Anisotropy_V3"],
            ["NormalZ_V1", "NormalZ_V2", "NormalZ_V3"]]

fig, axes = plt.subplots(4, 3)
subplot_idx = 0
for multi_feature in features:
    quantiles = labeled_pc_df[multi_feature].quantile([0.05, 0.95])

    min_X = quantiles.iloc[0, :].min()
    max_X = quantiles.iloc[1, :].max()
    X_plot = np.linspace(min_X, max_X, 1000).reshape(-1, 1)

    for feature_version in multi_feature:
        pivot_df = labeled_pc_df.pivot(columns='label', values=feature_version)

        current_version_class_kde = []
        for current_class_idx in range(len(pivot_df.columns)):
            X = pivot_df.iloc[:, current_class_idx].to_numpy()
            X = X[~np.isnan(X)].reshape(-1, 1)
            kde = KernelDensity(kernel='cosine', bandwidth=0.1).fit(X)

            current_version_class_kde.append(kde)

    for i in itertools.combinations(multi_feature, 2):
        # iterate over all possible version combinations
        for current_class_idx in range(len(pivot_df.columns)):
            # plot the difference per class for the current combination

            log_dens = kde.score_samples(X_plot)

            axes[np.unravel_index(subplot_idx, (4, 3))].plot(X_plot, np.exp(log_dens))
            plt.grid()


    subplot_idx += 1

plt.savefig(join(plot_out_path, 'feature_kernel_comparison.pdf'), bbox_inches='tight')

# ----------------------------------------------- Plot stacked histograms ----------------------------------------------
features = ["EchoWidth", "Reflectance", "NormalZ_V0", "Linearity_V0", "Planarity_V0", "Anisotropy_V0",
            "NormalizedZ"]

fig, axes = plt.subplots(np.ceil(len(features) / number_subplot_columns).astype(int), number_subplot_columns,
                         figsize=(12, 8))
for idx, current_feature in enumerate(features):
    pivot_df = labeled_pc_df.pivot(columns='label', values=current_feature)

    axes[idx // 3, idx % 3].set_title(current_feature)
    pivot_df.plot.hist(ax=axes[idx // 3, idx % 3], stacked=True, density=True, bins=40)
    if idx > 0:
        axes[idx // 3, idx % 3].get_legend().remove()

handles, labels = axes[0, 0].get_legend_handles_labels()
fig.legend(handles, labels, loc='upper center')
plt.savefig(join(plot_out_path, 'feature_histo_comparison.pdf'), bbox_inches='tight')

# -------------------------------------------- Correlation of the features ---------------------------------------------

fig, ax = plt.subplots(1, 1)
img = ax.matshow(abs(labeled_pc_df[features].corr()), cmap="Reds")
plt.colorbar(img)
ax.set_xticklabels(features)
ax.set_yticklabels(features)
plt.xticks(range(0, len(features)), rotation=45, horizontalalignment="right")
plt.yticks(range(0, len(features)))

plt.savefig(join(plot_out_path, 'feature_correlation.pdf'), bbox_inches='tight')
plt.show()

# ------------------------------------------ Compare features of Road vs Grass -----------------------------------------
class_names = ['Trees', 'Grass', 'Street', 'Vineyard', 'Flat roof', 'Gable roof']

plt.figure()
plt.hist(labeled_pc_df.loc[labeled_pc_df['label'] == 3, 'Reflectance'], density=True, bins=40, alpha=0.7)
plt.hist(labeled_pc_df.loc[labeled_pc_df['label'] == 2, 'Reflectance'], density=True, bins=40, alpha=0.7)
# make plot nicer
ax = plt.gca()
ax.legend(['Street', 'Grass'], fancybox=True)
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
ax.set_xlim([0, 0.5])
ax.hlines(np.arange(2, 10, 2), 0, 1, color="0.80", lw=0.5, zorder=-1)
ax.set_ylabel('Density')
ax.set_xlabel('Reflectivity')
plt.savefig(join(plot_out_path, 'reflectivity_grass_street.pdf'), bbox_inches='tight')

plt.figure()
plt.hist(labeled_pc_df.loc[labeled_pc_df['label'] == 3, 'Planarity_V0'], density=True, bins=40, alpha=0.7)
plt.hist(labeled_pc_df.loc[labeled_pc_df['label'] == 2, 'Planarity_V0'], density=True, bins=40, alpha=0.7)
# make plot nicer
ax = plt.gca()
ax.legend(['Street', 'Grass'], fancybox=True, loc="upper left")
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
# ax.set_xlim([0, 0.5])
ax.hlines(np.arange(1, 4, 1), 0, 1, color="0.80", lw=0.5, zorder=-1)
ax.set_ylabel('Density')
ax.set_xlabel('Planarity')
plt.savefig(join(plot_out_path, 'Planarity_V0_grass_street.pdf'), bbox_inches='tight')

# ------------------------------------------ Compare features of Road vs Grass -----------------------------------------
class_names = ['Trees', 'Grass', 'Street', 'Vineyard', 'Flat roof', 'Gable roof']

plt.figure()
plt.hist(labeled_pc_df.loc[labeled_pc_df['label'] == 5, 'NormalZ_V0'], density=True, bins=40, alpha=0.7)
plt.hist(labeled_pc_df.loc[labeled_pc_df['label'] == 6, 'NormalZ_V0'], density=True, bins=40, alpha=0.7)
# make plot nicer
ax = plt.gca()
ax.legend(['Flat roof', 'Gable roof'], fancybox=True, loc="upper left")
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
# ax.set_xlim([0, 0.5])
ax.hlines(np.arange(5, 31, 5), 0, 1, color="0.80", lw=0.5, zorder=-1)
ax.set_ylabel('Density')
ax.set_xlabel('NormalZ')
plt.savefig(join(plot_out_path, 'NormalZ_V0_flat_gable.pdf'), bbox_inches='tight')
