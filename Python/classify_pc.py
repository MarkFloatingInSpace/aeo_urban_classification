import pickle
import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier

pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_combined_versions.xyz"
NN_pc_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_classified_NN_v2.xyz"
NN_proba_pc_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_classified_proba_NN_v2.xyz"
DT_pc_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_classified_DT_v2.xyz"
NN_modell_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Python\NN_model_v2.sav"
DT_modell_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Python\DT_model_v2.sav"


type_dict = {'x': 'float32', 'y': 'float32', 'z': 'float32', "EchoWidth": 'float32', "EchoNumber": 'int8',
             "NrOfEchos": 'int8', "Reflectance": 'float32',
             "Linearity_V0": 'float32', "Planarity_V0": 'float32', "Anisotropy_V0": 'float32', "NormalZ_V0": 'float32',
             "EchoRatio_V0": 'float32',
             "Linearity_V1": 'float32', "Planarity_V1": 'float32', "Anisotropy_V1": 'float32', "NormalZ_V1": 'float32',
             "EchoRatio_V1": 'float32',
             "Linearity_V2": 'float32', "Planarity_V2": 'float32', "Anisotropy_V2": 'float32', "NormalZ_V2": 'float32',
             "EchoRatio_V2": 'float32',
             "Linearity_V3": 'float32', "Planarity_V3": 'float32', "Anisotropy_V3": 'float32', "NormalZ_V3": 'float32',
             "EchoRatio_V3": 'float32',
             "NormalizedZ": 'float32', "Ground_classification": 'int8'}

pc_df = pd.read_csv(pc_path, delimiter="\t", skiprows=1,
                    names=["x", "y", "z", "EchoWidth",
                           "EchoNumber", "NrOfEchos", "Reflectance",
                           "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                           "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                           "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                           "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                           "NormalizedZ", "Ground_classification"], dtype=type_dict)

features = ["EchoWidth", "EchoNumber", "NrOfEchos", "Reflectance",
            "NormalizedZ", "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
            "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2"]

X = pc_df[features]

# ---------------------------------------------- NN --------------------------------------------------------------------
mlpc = pickle.load(open(NN_modell_path, 'rb'))
predicted_label = mlpc.predict(X)
pc_df['label'] = predicted_label

# pc_df_minimal = pc_df[['x', 'y', 'z', 'label']]
# pc_df_minimal.to_csv(NN_pc_out_path)

predicted_prob = mlpc.predict_proba(X)
predicted_prob = np.amax(predicted_prob, axis=1)
pc_df['proba'] = predicted_prob
pc_df_minimal = pc_df[['x', 'y', 'z', 'label', 'proba']]
pc_df_minimal.to_csv(NN_proba_pc_out_path)
# ---------------------------------------------- DT --------------------------------------------------------------------
# dt = pickle.load(open(DT_modell_path, 'rb'))
# predicted_label = dt.predict(X)
#
# pc_df['label'] = predicted_label
# # pc_df.to_csv(DT_pc_out_path)
# pc_df_minimal = pc_df[['x', 'y', 'z', 'label']]
# pc_df_minimal.to_csv(DT_pc_out_path, header=False)