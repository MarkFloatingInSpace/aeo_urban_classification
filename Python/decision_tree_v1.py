import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from os.path import join
from random import sample

from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn.inspection import permutation_importance
from sklearn.model_selection import GridSearchCV


# labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\labeled_points_segmented_wy.csv"
labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\all_strips_combined_clean.csv"
model_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Python\DT_model_v2.sav"
plot_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Plots\DT_v2"

type_dict = {'x': 'float32', 'y': 'float32', 'z': 'float32', "EchoWidth": 'float32', "EchoNumber": 'int8',
             "NrOfEchos": 'int8', "Reflectance": 'float32',
             "Linearity_V0": 'float32', "Planarity_V0": 'float32', "Anisotropy_V0": 'float32', "NormalZ_V0": 'float32',
             "EchoRatio_V0": 'float32',
             "Linearity_V1": 'float32', "Planarity_V1": 'float32', "Anisotropy_V1": 'float32', "NormalZ_V1": 'float32',
             "EchoRatio_V1": 'float32',
             "Linearity_V2": 'float32', "Planarity_V2": 'float32', "Anisotropy_V2": 'float32', "NormalZ_V2": 'float32',
             "EchoRatio_V2": 'float32',
             "Linearity_V3": 'float32', "Planarity_V3": 'float32', "Anisotropy_V3": 'float32', "NormalZ_V3": 'float32',
             "EchoRatio_V3": 'float32',
             "NormalizedZ": 'float32', "Ground_classification": 'int8', 'label': 'int8'}

labeled_pc_df = pd.read_csv(labeled_pc_path, delimiter=",", skiprows=1,
                            names=["x", "y", "z", "EchoWidth",
                                   "EchoNumber", "NrOfEchos", "Reflectance",
                                   "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                                   "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                                   "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                                   "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                                   "NormalizedZ", "Ground_classification", "label"], dtype=type_dict)

# remove gravel class
labeled_pc_df = labeled_pc_df[labeled_pc_df.label != 7]


# -----------------------------------------------------Prepare data ----------------------------------------------------
# remove some tree points:
tree_idx = labeled_pc_df.index[labeled_pc_df['label'] == 1].tolist()
keep_idx = sample(tree_idx, 450000)
delete_idx = np.setdiff1d(tree_idx, keep_idx)
labeled_pc_df = labeled_pc_df.drop(delete_idx)

train, test = train_test_split(labeled_pc_df, test_size=0.4)
print("Training size: {}; Test size {}".format(len(train), len(test)))

features = ["EchoWidth", "EchoNumber", "NrOfEchos", "Reflectance", "NormalizedZ",
            "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
            "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2"]

class_names = ['Tree', 'Grass', 'Street', 'Vineyard', 'Flat roof', 'Gable roof']

X_train = train[features]
Y_train = train['label']

X_test = test[features]
Y_test = test['label']

# ---------------------------------------------- Tune and train DT -----------------------------------------------------

tuned_parameters = [{'criterion': ['gini', 'entropy'],
                     'max_depth': [8, 12, 16, 20, 24],
                     'min_samples_split': [100]}]

score = 'f1_weighted'

print("# Tuning hyper-parameters for %s" % score)
print()

dt_object = GridSearchCV(
    DecisionTreeClassifier(), tuned_parameters, scoring='%s' % score, n_jobs=3
)
dt_object.fit(X_train, Y_train)

pickle.dump(dt_object, open(model_out_path, 'wb'))

# ------------------------------------- Plot the score for all in a matrix ---------------------------------------------
layer_settings = tuned_parameters[0]['max_depth']
num_layer_settings = len(layer_settings)
act_settings = tuned_parameters[0]['criterion']
num_alpha_settings = len(act_settings)

cv_score_matrix = np.zeros((num_alpha_settings, num_layer_settings))

means = dt_object.cv_results_['mean_test_score']
stds = dt_object.cv_results_['std_test_score']
for mean, std, params in zip(means, stds, dt_object.cv_results_['params']):
    x_plot_idx = layer_settings.index(params['max_depth'])
    y_plot_idx = act_settings.index(params['criterion'])

    cv_score_matrix[y_plot_idx, x_plot_idx] = mean

fig = plt.figure()
ax = plt.axes()
ax.matshow(cv_score_matrix, cmap='winter')  # vmin=0.6, vmax=1
for (i, j), z in np.ndenumerate(cv_score_matrix):
    ax.text(j, i, '{:0.1f}'.format(z * 100), ha='center', va='center')

ax.xaxis.set_ticks_position('bottom')

x_tick_labels = [str(i) for i in layer_settings]
plt.xticks(range(0, num_layer_settings), x_tick_labels)
y_tick_labels = [str(i) for i in act_settings]
plt.yticks(range(0, num_alpha_settings), y_tick_labels)

plt.ylabel('Split criterion')
plt.xlabel('Maximum depth')
plt.savefig(join(plot_out_path, 'grid_search_F1_matrix.pdf'), bbox_inches='tight')


# ------------------------------------------------- Test trained DT ----------------------------------------------------
Y_pred = dt_object.predict(X_test)
score = accuracy_score(Y_test, Y_pred) * 100
print('Accuracy using decision tree: ', round(score, 1), '%')


# --------------------------------------- Plot Confusion Matrix and Loss Curve -----------------------------------------
cm = confusion_matrix(Y_test, Y_pred)

fig = plt.figure()
ax = plt.axes()
disp = plot_confusion_matrix(dt_object, X_test, Y_test,
                             display_labels=class_names,
                             cmap=plt.cm.Blues,
                             normalize='true',
                             ax=ax,
                             values_format='.3f')

plt.xticks(range(0, len(class_names)), class_names, rotation=45, horizontalalignment="right")
plt.tight_layout()
plt.savefig(join(plot_out_path, 'confusion_matrix_DT.pdf'), bbox_inches='tight')

# ------------------------------------------- Feature importance -------------------------------------------------------
# compute permutation feature importance
feature_importance = permutation_importance(dt_object, X_train, Y_train, n_repeats=20, random_state=42)
# get the indices of the sorted importance values
perm_sorted_idx = feature_importance.importances_mean.argsort()
perm_sorted_idx = perm_sorted_idx[::-1]

x_ticks = np.arange(0, len(features)) + 0.5
plt.figure()
plt.bar(x_ticks, feature_importance.importances_mean[perm_sorted_idx], alpha=0.8)
plt.xticks(x_ticks, [features[i] for i in perm_sorted_idx], rotation=45, horizontalalignment="right")
# plt.xlim((0, len(features)))

ax = plt.gca()
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
hist_max = np.amax(feature_importance.importances_mean)
ax.hlines(np.arange(0, 0.41, 0.05), -1, 15, color="0.80", lw=0.5, zorder=-1)
plt.xlim([-0.25, 15.25])
plt.ylabel('Permutation feature importance\naverage F1 score loss')
plt.xlabel('Feature')

plt.tight_layout()
plt.savefig(join(plot_out_path, 'feature_importance_DT.pdf'), bbox_inches='tight')
