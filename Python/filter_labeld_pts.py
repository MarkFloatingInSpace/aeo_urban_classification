import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\all_strips_combined_versions.xyz"
out_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\all_strips_combined_clean.csv"
pc_label_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\df_all_strips_labels.csv"

type_dict = {'x': 'float32', 'y': 'float32', 'z': 'float32', "EchoWidth": 'float32', "EchoNumber": 'int8',
             "NrOfEchos": 'int8', "Reflectance": 'float32',
             "Linearity_V0": 'float32', "Planarity_V0": 'float32', "Anisotropy_V0": 'float32', "NormalZ_V0": 'float32',
             "EchoRatio_V0": 'float32',
             "Linearity_V1": 'float32', "Planarity_V1": 'float32', "Anisotropy_V1": 'float32', "NormalZ_V1": 'float32',
             "EchoRatio_V1": 'float32',
             "Linearity_V2": 'float32', "Planarity_V2": 'float32', "Anisotropy_V2": 'float32', "NormalZ_V2": 'float32',
             "EchoRatio_V2": 'float32',
             "Linearity_V3": 'float32', "Planarity_V3": 'float32', "Anisotropy_V3": 'float32', "NormalZ_V3": 'float32',
             "EchoRatio_V3": 'float32',
             "NormalizedZ": 'float32', "Ground_classification": 'int8'}

pc_df = pd.read_csv(pc_path, delimiter="\t", skiprows=1,
                    names=["x", "y", "z", "EchoWidth",
                           "EchoNumber", "NrOfEchos", "Reflectance",
                           "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                           "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                           "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                           "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                           "NormalizedZ", "Ground_classification"], dtype=type_dict)

pc_class_labels_df = pd.read_csv(pc_label_path, delimiter=",", usecols=["x", "y", "index", "label"],
                                 dtype={'x': 'float32', 'y': 'float32', 'index': 'int32', 'label': 'int8'})
pc_class_labels_df.set_index("index", inplace=True)

# --------------------------------------------------------------------------------
# Combine .xyz data with class labels
pc_df = pd.concat([pc_df, pc_class_labels_df["label"]], axis=1, join='inner')

# Filter points with labels depending on the ground classification
# Labels
# 1 Tree
# 2 Grass
# 3 Street
# 4 Vineyard
# 5 flat roof
# 6 normal roof
# 7 gravel
# ground = 2, non ground = 0

# Show distribution of ground/non ground points per class
pivot_df = pc_df.pivot(columns='label', values="Ground_classification")
pivot_df.plot.hist(stacked=True, density=True, bins=2)

# Delete all ground points labeled as tree (1)
ground_only_classes = [2, 3, 7]
non_ground_only_classes = [1, 4, 5, 6]

for ground_only_class in ground_only_classes:
    pc_df.drop(pc_df[(pc_df["label"] == ground_only_class) & (pc_df["Ground_classification"] == 0)].index, inplace=True)

for non_ground_only_class in non_ground_only_classes:
    pc_df.drop(pc_df[(pc_df["label"] == non_ground_only_class) & (pc_df["Ground_classification"] == 2)].index, inplace=True)

pc_df.to_csv(out_pc_path)