import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os

import sklearn
from sklearn.cluster import KMeans
import sklearn.metrics as sm
from sklearn.preprocessing import scale

out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data"
labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\labeled_points_raw.csv"
labeled_pc_df = pd.read_csv(labeled_pc_path)

# tree_pc_df = labeled_pc_df[labeled_pc_df['class'] == 1]
# tree_pc_df = tree_pc_df.iloc[range(1, tree_pc_df.shape[0],2)]

labeled_pc_df.reset_index(inplace=True)
tree_class_indices = labeled_pc_df.index[labeled_pc_df['class'] == 1].tolist()
tree_class_indices = np.array(tree_class_indices)

features = ["EchoWidth", "NormalZ", "Planarity", "NormalizedZ", "RelEchoNumber"]
X = labeled_pc_df.loc[tree_class_indices, features]

# normalize the data
X = scale(X)

clustering = KMeans(n_clusters=2, random_state=10)
clustering.fit(X)

tree_pc_df = labeled_pc_df.iloc[tree_class_indices]

plt.subplot(2,2,1)
plt.scatter(tree_pc_df['NormalizedZ'], tree_pc_df['EchoWidth'], c=clustering.labels_)
plt.subplot(2,2,2)
plt.scatter(tree_pc_df['NormalizedZ'], tree_pc_df['Planarity'], c=clustering.labels_)
plt.subplot(2,2,3)
plt.scatter(tree_pc_df['NormalizedZ'], tree_pc_df['NormalZ'], c=clustering.labels_)
plt.subplot(2,2,4)
plt.scatter(tree_pc_df['NormalizedZ'], tree_pc_df['RelEchoNumber'], c=clustering.labels_)

plt_h = plt.figure().gca(projection='3d')
plt_h.scatter(tree_pc_df['NormalizedZ'], tree_pc_df['Reflectance'], tree_pc_df['Planarity'], c=clustering.labels_)

plt_h = plt.figure().gca(projection='3d')
subset_indices = range(0, tree_pc_df.shape[0], 25)
subset_points = tree_pc_df.iloc[subset_indices]
plt_h.scatter(subset_points.x, subset_points.y, subset_points.z, c=clustering.labels_[subset_indices])

# get the class which belongs to the centroid with the lower NormalizedZ value --> not trees
non_tree_clustering_idx = np.argmin(clustering.cluster_centers_[:, 3])
false_tree_class_indices = tree_class_indices[clustering.labels_ == np.argmin(clustering.cluster_centers_[:, 3])]

labeled_pc_df.drop(false_tree_class_indices, axis=0, inplace=True)
labeled_pc_df.to_csv(os.path.join(out_path, "labeled_points_segmented_trees.csv"))

plt.show()