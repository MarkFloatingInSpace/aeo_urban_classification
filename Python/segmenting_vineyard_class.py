import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os

from sklearn.cluster import KMeans
from sklearn.preprocessing import scale

out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data"
labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\labeled_points_segmented_trees.csv"
labeled_pc_df = pd.read_csv(labeled_pc_path)

labeled_pc_df.reset_index(inplace=True)

wy_class_corr_height_indices = labeled_pc_df.index[(labeled_pc_df['class'] == 4) & (labeled_pc_df['NormalizedZ'] <= 3)].tolist()
wy_class_corr_height_indices = np.array(wy_class_corr_height_indices)

wy_class_too_high_indices = labeled_pc_df.index[(labeled_pc_df['class'] == 4) & (labeled_pc_df['NormalizedZ'] > 3)].tolist()

# vineyard_pc_df = vineyard_pc_df.iloc[range(1, vineyard_pc_df.shape[0],2)]

features = ["EchoWidth", "NormalizedZ", 'Planarity']
X = labeled_pc_df.loc[wy_class_corr_height_indices, features]

# normalize the data
X = scale(X)

clustering = KMeans(n_clusters=2, random_state=10)
clustering.fit(X)

vineyard_pc_df = labeled_pc_df.iloc[wy_class_corr_height_indices]

plt.subplot(2,2,1)
plt.scatter(vineyard_pc_df['NormalizedZ'], vineyard_pc_df['EchoWidth'], c=clustering.labels_)
plt.subplot(2,2,2)
plt.scatter(vineyard_pc_df['NormalizedZ'], vineyard_pc_df['Planarity'], c=clustering.labels_)
plt.subplot(2,2,3)
plt.scatter(vineyard_pc_df['NormalizedZ'], vineyard_pc_df['NormalZ'], c=clustering.labels_)
plt.subplot(2,2,4)
plt.scatter(vineyard_pc_df['NormalizedZ'], vineyard_pc_df['RelEchoNumber'], c=clustering.labels_)

plt_h = plt.figure().gca(projection='3d')
plt_h.scatter(vineyard_pc_df['x'], vineyard_pc_df['y'], vineyard_pc_df['z'], c=clustering.labels_)
plt.show()

# get the class which belongs to the centroid with the lower NormalizedZ value --> not trees
non_wy_clustering_idx = np.argmin(clustering.cluster_centers_[:, 1])
false_wy_class_indices = wy_class_corr_height_indices[clustering.labels_ == non_wy_clustering_idx]

labeled_pc_df.drop(wy_class_too_high_indices, axis=0, inplace=True)
labeled_pc_df.drop(false_wy_class_indices, axis=0, inplace=True)
labeled_pc_df.to_csv(os.path.join(out_path, "labeled_points_segmented_wy.csv"))