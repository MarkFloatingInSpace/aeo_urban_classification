import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
from os.path import join

plot_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Plots\NN"
NN_proba_pc_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_classified_proba_NN.xyz"
pc_df = pd.read_csv(NN_proba_pc_out_path)

class_names = ['Trees', 'Grass', 'Street', 'Vineyard', 'Flat roof', 'Gable roof']

pivot_df = pc_df.pivot(columns="label", values="proba")

fig, axes = plt.subplots(2, 3, figsize=(7,4))

for idx in range(axes.size):
    ax = axes[np.unravel_index(idx, axes.shape)]

    hist_data = ax.hist(pivot_df[idx+1]*100, density=True, bins=50)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.set_title(class_names[idx])
    ax.set_xlim([0, 100])
    hist_max = np.amax(hist_data[0])
    magnitude = math.floor(math.log(hist_max, 10))
    ax.hlines(np.linspace(0, hist_max, 5), 0, 100, color="0.80", lw=0.5)

fig.text(0.5, 0.005, 'Classification probability [%]', ha='center')
fig.text(0.005, 0.5, 'Density', va='center', rotation='vertical')
plt.tight_layout()
plt.show()
plt.savefig(join(plot_out_path, 'NN_classification_proba.pdf'), bbox_inches='tight')
