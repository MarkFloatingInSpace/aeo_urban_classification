import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle
from os.path import join
from random import sample

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.neural_network import MLPClassifier
from sklearn.inspection import permutation_importance
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import plot_confusion_matrix

labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\all_strips_combined_clean.csv"
model_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Python\NN_model_v2.sav"
plot_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Plots\NN_v2"

type_dict = {'x': 'float32', 'y': 'float32', 'z': 'float32', "EchoWidth": 'float32', "EchoNumber": 'int8',
             "NrOfEchos": 'int8', "Reflectance": 'float32',
             "Linearity_V0": 'float32', "Planarity_V0": 'float32', "Anisotropy_V0": 'float32', "NormalZ_V0": 'float32',
             "EchoRatio_V0": 'float32',
             "Linearity_V1": 'float32', "Planarity_V1": 'float32', "Anisotropy_V1": 'float32', "NormalZ_V1": 'float32',
             "EchoRatio_V1": 'float32',
             "Linearity_V2": 'float32', "Planarity_V2": 'float32', "Anisotropy_V2": 'float32', "NormalZ_V2": 'float32',
             "EchoRatio_V2": 'float32',
             "Linearity_V3": 'float32', "Planarity_V3": 'float32', "Anisotropy_V3": 'float32', "NormalZ_V3": 'float32',
             "EchoRatio_V3": 'float32',
             "NormalizedZ": 'float32', "Ground_classification": 'int8', 'label': 'int8'}


labeled_pc_df = pd.read_csv(labeled_pc_path, delimiter=",", skiprows=1,
                            names=["x", "y", "z", "EchoWidth",
                                   "EchoNumber", "NrOfEchos", "Reflectance",
                                   "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                                   "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                                   "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                                   "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                                   "NormalizedZ", "Ground_classification", "label"], dtype=type_dict)

# remove gravel class
labeled_pc_df = labeled_pc_df[labeled_pc_df.label != 7]

class_names = ['Tree', 'Grass', 'Street', 'Vineyard', 'Flat roof', 'Gable roof']

# ------------------------------------------- Plot class distribution --------------------------------------------------
plt.figure()
bins = np.arange(1, len(class_names) + 1.5) - 0.5
# hist_data = plt.hist(labeled_pc_df['label'], bins, rwidth=0.85)


# thin out pc
# labeled_pc_df = labeled_pc_df.iloc[range(0, labeled_pc_df.shape[0], 20)]

# remove some tree points:
tree_idx = labeled_pc_df.index[labeled_pc_df['label'] == 1].tolist()
keep_idx = sample(tree_idx, 450000)
delete_idx = np.setdiff1d(tree_idx, keep_idx)
labeled_pc_df = labeled_pc_df.drop(delete_idx)

hist_data = plt.hist(labeled_pc_df['label'], bins, rwidth=0.85, alpha=0.8)
plt.xticks(range(1, len(class_names) + 1), class_names, rotation=45, horizontalalignment="right")
plt.xlabel('Class')
plt.ylabel('Number of samples')

ax = plt.gca()
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
hist_max = np.amax(hist_data[0])
ax.hlines(np.arange(0, 4e5+1, 1e5), 0.25, 6.75, color="0.80", lw=0.5, zorder=-1)
plt.xlim([0.25, 6.75])
plt.tight_layout()
plt.savefig(join(plot_out_path, 'number_obs_per_class.pdf'), bbox_inches='tight')
plt.show()
# ------------------------------------------- Select features and split data -------------------------------------------

features = ["EchoWidth", "EchoNumber", "NrOfEchos", "Reflectance", "NormalizedZ",
            "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
            "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2"]

X = labeled_pc_df[features]
y = labeled_pc_df['label']

# Split the dataset in two equal parts
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.4, random_state=0)

# ---------------------------------------------- Tune and Train NN -----------------------------------------------------
# Set the parameters by cross-validation
tuned_parameters = [{'hidden_layer_sizes': [(), (11), (13), (15), (15, 13), (15, 11)],
                     'alpha': [1e-5, 1e-3, 1e-1],
                     'activation': ['tanh'],
                     'learning_rate': ['adaptive'],
                     'early_stopping': [True],
                     'max_iter': [500]}]

score = 'f1_weighted'

print("# Tuning hyper-parameters for %s" % score)
print()

mlpc = GridSearchCV(
    MLPClassifier(), tuned_parameters, scoring='%s' % score, n_jobs=3
)
mlpc.fit(X_train, y_train)
pickle.dump(mlpc, open(model_out_path, 'wb'))

print("Best parameters set found on development set:")
print()
print(mlpc.best_params_)
print()
print("Grid scores on development set:")
print()
means = mlpc.cv_results_['mean_test_score']
stds = mlpc.cv_results_['std_test_score']
for mean, std, params in zip(means, stds, mlpc.cv_results_['params']):
    print("%0.3f (+/-%0.03f) for %r"
          % (mean, std * 2, params))
print()

# --------------------------------------------- Test trained NN --------------------------------------------------------
print("Detailed classification report:")
print()
print("The model is trained on the full development set.")
print("The scores are computed on the full evaluation set.")
print()
y_true, y_pred = y_test, mlpc.predict(X_test)
print(classification_report(y_true, y_pred))
print()

# --------------------------------------- Plot Confusion Matrix and Loss Curve -----------------------------------------
cm = confusion_matrix(y_true, y_pred)

fig = plt.figure()
ax = plt.axes()
disp = plot_confusion_matrix(mlpc, X_test, y_true,
                             display_labels=class_names,
                             cmap=plt.cm.Blues,
                             normalize='true',
                             ax=ax,
                             values_format='.3f')

plt.xticks(range(0, len(class_names)), class_names, rotation=45, horizontalalignment="right")

# disp.ax_.set_title('Normalized confusion matrix NN')
plt.tight_layout()
plt.savefig(join(plot_out_path, 'confusion_matrix_NN.pdf'), bbox_inches='tight')

# plt.figure()
# plt.plot(mlpc.loss_curve_)
# plt.title('Loss curve')
# plt.xlabel('Iteration')
# plt.ylabel('Loss')

# ------------------------------------- Plot the score for all in a matrix ---------------------------------------------
layer_settings = tuned_parameters[0]['hidden_layer_sizes']
num_layer_settings = len(layer_settings)
act_settings = tuned_parameters[0]['alpha']
num_alpha_settings = len(act_settings)

cv_score_matrix = np.zeros((num_alpha_settings, num_layer_settings))

means = mlpc.cv_results_['mean_test_score']
stds = mlpc.cv_results_['std_test_score']
for mean, std, params in zip(means, stds, mlpc.cv_results_['params']):
    x_plot_idx = layer_settings.index(params['hidden_layer_sizes'])
    y_plot_idx = act_settings.index(params['alpha'])

    cv_score_matrix[y_plot_idx, x_plot_idx] = mean

fig = plt.figure()
ax = plt.axes()
ax.matshow(cv_score_matrix, cmap='winter')  # vmin=0.6, vmax=1
for (i, j), z in np.ndenumerate(cv_score_matrix):
    ax.text(j, i, '{:0.1f}'.format(z * 100), ha='center', va='center')

ax.xaxis.set_ticks_position('bottom')

x_tick_labels = [str(i) for i in layer_settings]
plt.xticks(range(0, num_layer_settings), x_tick_labels, rotation=45, horizontalalignment="right")
y_tick_labels = [str(i) for i in act_settings]
plt.yticks(range(0, num_alpha_settings), y_tick_labels)

plt.ylabel('Alpha')
plt.xlabel('Hidden layer sizes')
plt.savefig(join(plot_out_path, 'grid_search_F1_matrix.pdf'), bbox_inches='tight')

# ------------------------------------------- Feature importance -------------------------------------------------------
# compute permutation feature importance
feature_importance = permutation_importance(mlpc, X_test, y_test, n_repeats=20, random_state=42, scoring='f1_weighted')
# get the indices of the sorted importance values
perm_sorted_idx = feature_importance.importances_mean.argsort()
perm_sorted_idx = perm_sorted_idx[::-1]

x_ticks = np.arange(0, len(features)) + 0.5
plt.figure()
plt.bar(x_ticks, feature_importance.importances_mean[perm_sorted_idx], alpha=0.8)
plt.xticks(x_ticks, [features[i] for i in perm_sorted_idx], rotation=45, horizontalalignment="right")
# plt.xlim((0, len(features)))

ax = plt.gca()
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
hist_max = np.amax(feature_importance.importances_mean)
ax.hlines(np.arange(0, 0.31, 0.05), -1, 15, color="0.80", lw=0.5, zorder=-1)
plt.xlim([-0.25, 15.25])
plt.ylabel('Permutation feature importance\naverage F1 score loss')
plt.xlabel('Feature')

plt.tight_layout()

plt.savefig(join(plot_out_path, 'feature_importance_NN.pdf'), bbox_inches='tight')

plt.show()
