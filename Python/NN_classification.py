import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import plot_confusion_matrix
from sklearn.neural_network import MLPClassifier
from sklearn.inspection import permutation_importance

# labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\labeled_points_segmented_wy.csv"
# labeled_pc_df = pd.read_csv(labeled_pc_path)

labeled_pc_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Data\pts\strip1_combined_clean_less_trees.csv"
model_out_path = r"Y:\AEO_urban_classification\aeo_urban_classification\Python\NN_model_less_trees.sav"

labeled_pc_df = pd.read_csv(labeled_pc_path, delimiter=",", skiprows=1,
                            names=["x", "y", "z", "EchoWidth",
                                   "EchoNumber", "NrOfEchos", "Reflectance",
                                   "Linearity_V0", "Planarity_V0", "Anisotropy_V0", "NormalZ_V0", "EchoRatio_V0",
                                   "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
                                   "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2",
                                   "Linearity_V3", "Planarity_V3", "Anisotropy_V3", "NormalZ_V3", "EchoRatio_V3",
                                   "NormalizedZ", "Ground_classification", "label"])



train, test = train_test_split(labeled_pc_df, test_size=0.30)
print("Training size: {}; Test size {}".format(len(train), len(test)))

features = ["EchoWidth", "EchoNumber", "NrOfEchos", "Reflectance",
            "NormalizedZ", "Linearity_V1", "Planarity_V1", "Anisotropy_V1", "NormalZ_V1", "EchoRatio_V1",
            "Linearity_V2", "Planarity_V2", "Anisotropy_V2", "NormalZ_V2", "EchoRatio_V2"]

X_train = train[features]
Y_train = train['label']

X_test = test[features]
Y_test = test['label']

mlpc = MLPClassifier(hidden_layer_sizes=(15, 15, 15), max_iter=1000,
                     activation='relu',
                     learning_rate='adaptive',
                     early_stopping=True)
mlpc.fit(X_train, Y_train)
pickle.dump(mlpc, open(model_out_path, 'wb'))

Y_pred = mlpc.predict(X_test)
score = accuracy_score(Y_test, Y_pred) * 100
print('Accuracy using decision tree: ', round(score, 1), '%')

cm = confusion_matrix(Y_test, Y_pred)

class_names = ['Wald', 'Wiese', 'Straße', 'Weingärten', 'Häuser flach', 'Häuser Giebel']

disp = plot_confusion_matrix(mlpc, X_test, Y_test,
                             display_labels=class_names,
                             cmap=plt.cm.Blues,
                             normalize='true')

disp.ax_.set_title('Normalized confusion matrix NN')

plt.figure()
plt.plot(mlpc.loss_curve_)
plt.title('Loss curve')
plt.xlabel('Iteration')
plt.ylabel('Loss')

# ------------------------------------------- Feature importance -------------------------------------------------------
# compute permutation feature importance
feature_importance = permutation_importance(mlpc, X_train, Y_train, n_repeats=20, random_state=42, scoring='f1_weighted')
# get the indices of the sorted importance values
perm_sorted_idx = feature_importance.importances_mean.argsort()

y_ticks = np.arange(0, len(features)) + 0.5
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
ax1.barh(y_ticks, feature_importance.importances_mean[perm_sorted_idx], height=0.7)
ax1.set_yticks(y_ticks)
ax1.set_yticklabels([features[i] for i in perm_sorted_idx])
# ax1.set_yticks(tree_indices)
ax1.set_ylim((0, len(features)))
ax2.boxplot(feature_importance.importances[perm_sorted_idx].T, vert=False,
            labels=[features[i] for i in perm_sorted_idx])
fig.tight_layout()
plt.show()